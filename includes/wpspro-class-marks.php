<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Public Pages Class
 * 
 * Handles all the different features of marks module
 * for the front end pages.
 * 
 * @package  WPSchoolPressPro
 * @since 1.0.0
 */
class Wpspro_Marks {
	public function wpspro_get_marks() {
		global $wpdb;
		$class_id	=	$_POST['ClassID'];
		$subject_id	=	$_POST['SubjectID'];
		$exam_id	=	$_POST['ExamID'];
		$finalarray	=	array();		
		$extra_tbl		=	$wpdb->prefix."wpsp_mark_fields";
		$student_table	=	$wpdb->prefix."wpsp_student";
		$mtable			=	$wpdb->prefix."wpsp_mark";
		$exmark_tbl		=	$wpdb->prefix."wpsp_mark_extract";
			
		$exportcolumn 	=	array('Sno','Rollno','Name','Mark');
		$extra_fields	=	$wpdb->get_results("select * from $extra_tbl where subject_id='$subject_id'");			
		if(!empty($extra_fields)){
			foreach($extra_fields as $extf){
				array_push($exportcolumn,$extf->field_text);
			}
		}
		
		$wpsp_marks		=	$wpdb->get_results("select * from $mtable WHERE subject_id=$subject_id and class_id=$class_id and exam_id=$exam_id order by mid ASC");
		$wpsp_exmarks	=	$wpdb->get_results("select * from $exmark_tbl WHERE subject_id=$subject_id and exam_id=$exam_id");
		$class_students	=	$wpdb->get_results("select s_rollno,wp_usr_id,s_fname from $student_table where class_id='$class_id'",ARRAY_A);
		$students_list	=	$extra_marks	=	array();
		foreach($class_students as $cstud) {
			$students_list[$cstud['wp_usr_id']]=array('rollno'=>$cstud['s_rollno'],'name'=>$cstud['s_fname']);
		}
		foreach($wpsp_exmarks as $exmark){
			$extra_marks[$exmark->student_id][$exmark->field_id]=$exmark->mark;
		}
		$sno=1;
		foreach($wpsp_marks as $mark){
			$row=array();
			$row=array($sno,$students_list[$mark->student_id]['rollno'],$students_list[$mark->student_id]['name'],$mark->mark);
			if(!empty($extra_fields)){
				foreach($extra_fields as $extf){
					array_push($row,$extra_marks[$mark->student_id][$extf->field_id]);
				}
			}
			$finalarray[]=$row;
			$sno++;
		}
		$response['result'] 		= $finalarray;
		$response['exportcolumn'] 	= $exportcolumn;
		return $response;
	}
	
	public function wpspro_export_marks() {	
		if( isset( $_POST['exportmarks'] ) && $_POST['exportmarks']=='exportmarks' ) {	
			$csv_header	=	$csv_row	=	'';		
			$response		=	$this->wpspro_get_marks();	
			$finalarray		=	$response['result'];
			$exportcolumn	=	$response['exportcolumn'];				
			$csv_header     = 	implode( ",", $exportcolumn );
			$csv_header	   .= 	"\n";
			
			foreach($finalarray as $line ){
				if( is_array( $line ) ) {
					foreach( $line as $key=>$value ) {
						$csv_row .= '"' . $value . '",';
					}
				} else {
					$csv_row .= '"' . $line . '",';
				}
				$csv_row .= "\n";
			}		
			/* Download as CSV File */
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename=ExportMarks.csv');
			echo $csv_header . $csv_row;
			exit;
		}
	}
	
	function wpspro_mark_print() {
		$response		=	$this->wpspro_get_marks();
		$result			=	$response['result'];
		$exportcolumn	=	$response['exportcolumn'];
		$finalarray 	= 	json_decode(json_encode($result), True);
		if(count($finalarray) ) { 
			echo "<html>";
			echo "<head><title>Students Marks</title>";
			echo "<link href='".WPSPRO_PLUGIN_URL."includes/css/wpsp-print.css' rel='stylesheet' type='text/css' />";
			echo "</head><body>";
			echo "<table class='wpspro_print'>";
			foreach($exportcolumn as $header){
				echo "<th>".$header."</th>";
			}
			foreach($finalarray as $row){
				echo "<tr>";
				foreach($row as $column){
					echo "<td>".$column."</td>";
				}
				echo "</tr>";
			}
			echo "</table></body></html>";
		}
		exit();
	}
	
	
	function wpspro_marks_scripts() {
		if ( is_page( 'sch-marks' ) ) {
			echo "<link href='".WPSPRO_PLUGIN_URL."includes/css/wpsp-code-public.css' rel='stylesheet' type='text/css' />";
			echo "<script src='".WPSPRO_PLUGIN_URL . "includes/js/wpspro-marks-scripts.js'></script>";
		}
	}
	
	function wpspro_import_marks() {
		global $wpdb;	
		if( isset( $_POST['MarkAction'] ) && $_POST['MarkAction']=='ImportCSV' ) {
			//Import marks from CSV
			if( is_uploaded_file( $_FILES['MarkCSV']['tmp_name'] ) ) {
				$error_message	=	'';
				$ftext 			= 	$_FILES['MarkCSV']['name'];
				$ext 			=	pathinfo($ftext, PATHINFO_EXTENSION);
				if( $_FILES['MarkCSV']['size'] ==0 ) {					
					$error_message .=" <li>".__( 'Attached CSV file is Empty', 'WPSchoolPress')."</li>";				
				} 
				if( $ext!='csv' ) {
					$error_message .="<li>". __( 'Attached file is not a CSV', 'WPSchoolPress'). "</li>";				}
					$upload_dir =	wp_upload_dir();
					$basedir	=	$upload_dir['basedir'];
					$targetPath	=	$basedir.'/markcsv';
					if( !file_exists( $targetPath ) ) {
						mkdir($targetPath);				
					}
					$targetPath	=	$targetPath.'/'.$_FILES['MarkCSV']['name'];
					if($error_message=='' && (move_uploaded_file($_FILES['MarkCSV']['tmp_name'],$targetPath))) {
						$file=$targetPath;
						$file_read_error='';					
						if(($fp = fopen($file, 'r'))!== false) {
							$headings=fgetcsv($fp);	} 
						else{						
							echo $file_read_error= __( "File not readable or empty!", "WPSchoolPress");					}
						
						if(count($headings)==0) {						
							echo $file_read_error=__( "No headings found in attached csv", "WPSchoolPress");					}
						if( $file_read_error=='' ) {
							$class		=	$_POST['ClassID'];						
							$subject	=	$_POST['SubjectID'];						
							$exam		=	$_POST['ExamID'];												
							//Check whether mark is already entered
							if( wpsp_IsMarkEntered( $class,$subject,$exam ) ){
								echo "<div class='wpsp-col-md-12'><div class='alert alert-warning alert-flat'>".__( 'Marks were entered for this subject!', 'WPSchoolPress')."</div></div>";							
							
							}
							//Get Extra fields
							$extra_tbl		=	$wpdb->prefix."wpsp_mark_fields";
							$extra_fields	=	$wpdb->get_results("select * from $extra_tbl where subject_id='$subject'");		
							$importfields	=	array( 'RollNo' => 'RollNo', 'Name'=>'Student Name', 'Mark'=>'Mark')	
						?>	
							<div class="wpsp-col-md-12">							
								<form action='' class="wpsp-form-horizontal" name='mapcsv' method='post'>
									<input type="hidden" name="ClassID" value="<?php echo $class;?>">								
									<input type="hidden" name="SubjectID" value="<?php echo $subject;?>">								
									<input type="hidden" name="ExamID" value="<?php echo $exam;?>">								
									<input type="hidden" name="FilePath" value="<?php echo $targetPath;?>">	
									<?php foreach( $importfields as $key=>$value ) { ?>
										<div class="wpsp-form-group wpsp-col-md-4">
											<label class="wpsp-col-md-4 wpsp-label"><?php echo $key; ?><span class="wpsp-required">*</span></label>
											<div class="wpsp-col-md-8">
												<select name="<?php echo $key;?>" id="<?php echo $key;?>" class="wpsp-form-control" required>
													<option value=""><?php _e( 'Select CSV Column', 'WPSchoolPress');?> </option>
													<?php foreach( $headings as $key=>$value ) { ?>	
														<option value="<?php echo $key;?>"><?php echo $value;?></option>
													<?php } ?>											
												</select>			
											</div>
										</div>
									<?php } ?>
								<?php if(count($extra_fields)){
										foreach($extra_fields as $extf){ ?>
											<div class="wpsp-form-group wpsp-col-md-4">
												<label class="wpsp-col-md-4 wpsp-label"><?php echo $extf->field_text;?></label>
												<div class="wpsp-col-md-8">
													<select name="extra[<?php echo $extf->field_id;?>]" class="wpsp-form-control">
														<option value=""><?php _e( 'Select CSV Column', 'WPSchoolPress'); ?> </option>
														<?php	foreach($headings as $key=>$value) { ?>
																<option value="<?php echo $key;?>"><?php echo $value;?></option>
														<?php	}	?>
													</select>
												</div>
											</div>
								<?php }	 } ?>
									<div class="wpsp-form-group">
										<div class="wpsp-col-md-8 wpsp-col-md-offset-4">
											<input type="submit" class="wpsp-btn wpsp-btn-success wpsp-btn-flat" name="importmap" value="Submit">
										</div>
									</div>
								</form>						
							</div>
					<?php }
				} else {
						$error_message .=__( "Error while saving file!", "WPSchoolPress" );	}
			}
			if($_FILES['MarkCSV']['tmp_name']==''){
						_e( 'File not attached', 'WPSchoolPress' );
			}
		}else if( isset( $_POST['importmap'] ) ) {
			//Process csv file after mapping done											
			$class		=	$_POST['ClassID'];
			$subject	=	$_POST['SubjectID'];
			$exam		=	$_POST['ExamID'];
			$filePath	=	$_POST['FilePath'];
			$mapRoll	=	$_POST['RollNo'];
			$mapName	=	$_POST['Name'];
			$mapMark	=	$_POST['Mark'];
			$mapExtra	=	$not_inserted	=	$inserted_mids	=	$inserted_exmids	=	array();
			if( isset( $_POST['extra'] ) && count( $_POST['extra'] ) > 0 ) {
				$mapExtra=$_POST['extra'];
				array_filter($mapExtra);
			}
			$skip = true;
			$student_table		=	$wpdb->prefix."wpsp_student";
			$mark_table			=	$wpdb->prefix."wpsp_mark";
			$extra_mark_table	=	$wpdb->prefix."wpsp_mark_extract";
			$class_students		=	$wpdb->get_results("select s_rollno,wp_usr_id,s_fname from $student_table where class_id='$class'",ARRAY_A);
			$students_roll	=	$students_name	=	array();
									
			foreach( $class_students as $cstud ) {
				$students_roll[$cstud['wp_usr_id']]	=	trim($cstud['s_rollno']);
				$students_name[$cstud['wp_usr_id']]	=	strtolower(trim($cstud['s_fname']));
			}
									
			$file	=	fopen($filePath,"r");
			while( ( $markData = fgetcsv($file, 10000, ",") ) !== FALSE ) {
				//skip header row
				if($skip) { $skip = false; $headings=$markData; continue; }
										
					$csvRoll=$markData[$mapRoll];
					$csvName=$markData[$mapName];
					$csvMark=$markData[$mapMark];
					
					//CSV rollno,name along with db value and retrive student_id
					$student_id=array_search(trim($csvRoll),$students_roll);
					if( !$student_id>0 ) {
						$student_id=array_search(strtolower(trim($csvName)),$students_name);
					}
					
					if($student_id > 0){
						//check whether mark is inserted											
						$mcheck=$wpdb->get_row("select mid from $mark_table where student_id='$student_id' and subject_id='$subject' and class_id='$class' and exam_id='$exam'",ARRAY_A);
						if(count($mcheck)>0){
							$mid=$mcheck['mid'];
							$markUpdate=$wpdb->update($mark_table,array('mark'=>$csvMark),array('mid'=>$mid));
							$inserted_mids[]=$mid;
						} else {
							$markInsert=$wpdb->insert($mark_table,array('subject_id'=>$subject,'class_id'=>$class,'student_id'=>$student_id,'exam_id'=>$exam,'mark'=>$csvMark));												
							$inserted_mids[] = $markInsert;
						}
						
						//Mark extract table insert
						if(count($mapExtra)){
							foreach($mapExtra as $extraFid=>$extraMap){
								if( isset( $markData[$extraMap] ) && !empty ( $markData[$extraMap] ) ) {
									$exmarkcheck=$wpdb->get_row("select id from $extra_mark_table where 'student_id'='$student'_id and subject_id='$subject' and field_id='$extraFid' and 'exam_id'='$exam'");
									if(count($exmarkcheck)){
										$exmark_id=$exmarkcheck['id'];
										$extraUpdate=$wpdb->update($extra_mark_table,array('mark'=>$markData[$extraMap]),array('id'=>$exmark_id));
										$inserted_exmids[]=$exmark_id;
									} else {
										$extraInsert=$wpdb->insert($extra_mark_table,array('student_id'=>$student_id,'field_id'=>$extraFid,'exam_id'=>$exam,'subject_id'=>$subject,'mark'=>$markData[$extraMap]));
										$inserted_exmids[]=$extraInsert->id;
									}
								}
							}
						}
					} else{
						$not_inserted[]=$markData;
					}
			}
			
			//Create error file
			if( count( $not_inserted ) ) {
				echo "notinserted<pre>";
				echo '</pre>';
				$filename = "marknotinserted.csv";
				$fp = fopen($filename, 'w');
				fputcsv($fp, $headings);
				foreach($not_inserted as $row){
					fputcsv($fp,str_replace('"','',$row));
				}
					
		} else {
			echo 'Mark Imported Successfully';	
		}
	  }
	}
	
	public function add_hooks() {
		add_action( 'init', array( $this, 'wpspro_export_marks' ), 10 );
		add_action( 'wpsp_footer_script', array( $this, 'wpspro_marks_scripts' ) );
		add_action( 'wpsp_marks_actions', array( $this, 'wpspro_import_marks' ), 10 );
		add_action( 'wp_ajax_marksPrint', array( $this, 'wpspro_mark_print' ) );	
	}
}