jQuery(document).ready(function () {

	$('#PrintMarks').click(function(e) {		
		var columns=$('#MarkForm').serializeArray();
		columns.push({name: 'action', value: 'marksPrint'});
		jQuery.post(ajax_url, columns, function(pdata) {
			var HTML = pdata;			
			var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");		
			WindowObject.document.writeln(HTML);
			WindowObject.document.close();
			WindowObject.focus();
		});
	});
});