$(document).ready(function(){
	$('#PrintParent').click(function(){
		var columns=$('#ParentColumnForm').serializeArray();
		columns.push({name: 'action', value: 'ParentPrint'});
		jQuery.post(ajax_url, columns, function(pdata) {
			var HTML = pdata;
			var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
			WindowObject.document.writeln(HTML);
			WindowObject.document.close();
			WindowObject.focus();
			WindowObject.print();
			WindowObject.close();
		});
	});
	
	$('#ExportParents').click(function(){
		var columns=$('#ExportColumnForm').serializeArray();
		columns.push({name: 'action', value: 'exportparent'}); 
		$.ajax({
						type: "POST",
						url: ajax_url,
						data: columns,
						cache: false,
						processData: true,
						success: function(rdata) {
							var mainarrayvalue=[];
							 var titleheade = "";
							jQuery.each( jQuery.parseJSON(rdata), function( i, val ) {
							var subarray=[];
							var j=0;							
								jQuery.each( val, function( f, val1 ) {
									
									subarray[j]=val1;
									if(i ==0){
									 titleheade = titleheade + ','+f;
									}
									j++;
									});
									mainarrayvalue[i]=subarray;
										});

								var titleheademain = titleheade.substr(1);
									titleheademain=titleheademain+'\n'; 
 
							mainarrayvalue.forEach(function(row) {
								titleheademain += row.join(',');
								titleheademain += "\n";
								});
 
								var hiddenElement = document.createElement('a');
								hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(titleheademain);
								hiddenElement.target = '_blank';
								hiddenElement.download = 'Exportparent.csv';
								hiddenElement.click();
						}
					});
	});
});