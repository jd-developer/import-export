jQuery( document ).ready( function( $ ) {
	var lines = [];
	$('#ExportStudents').click(function(){
		var columns=$('#ExportColumnForm').serializeArray();
		columns.push({name: 'action', value: 'exportstudent'});
		$.ajax({
						type: "POST",
						url: ajax_url,
						data: columns,
						cache: false,
						processData: true,
						success: function(rdata) {
							var mainarrayvalue=[];
							 var titleheade = "";
							jQuery.each( jQuery.parseJSON(rdata), function( i, val ) {
							var subarray=[];
							var j=0;							
								jQuery.each( val, function( f, val1 ) {
									
									subarray[j]=val1;
									if(i ==0){
									 titleheade = titleheade + ','+f;
									}
									j++;
									});
									mainarrayvalue[i]=subarray;
										});

								var titleheademain = titleheade.substr(1);
									titleheademain=titleheademain+'\n'; 
 
 

							mainarrayvalue.forEach(function(row) {
								titleheademain += row.join(',');
								titleheademain += "\n";
								});
 
								var hiddenElement = document.createElement('a');
								hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(titleheademain);
								hiddenElement.target = '_blank';
								hiddenElement.download = 'ExportStudent.csv';
								hiddenElement.click();
						}
					});
	
	

	});
	
	$('#PrintStudent').click(function(){
		var columns=$('#StudentColumnForm').serializeArray();
		columns.push({name: 'action', value: 'StudentsPrint'});
		jQuery.post(ajax_url, columns, function(pdata) {
			var HTML = pdata;
			var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
			WindowObject.document.writeln(HTML);
			WindowObject.document.close();
			WindowObject.focus();
			WindowObject.print();
			WindowObject.close();
		});
	});
	
	/*$('#ImportStudent').on('click',function(e){
		e.preventDefault();
		$('#ImportModal').modal('show');
	});*/
	
	$('#importcsv').change(function(e) {
			var ext = $("input#importcsv").val().split(".").pop().toLowerCase();
			if($.inArray(ext, ["csv"]) == -1) {
				$.fn.notify('error',{'desc':'File format must be CSV!'});
				return false;
			}			
			if (e.target.files != undefined) {
				var reader = new FileReader();
				reader.onload = function(e) {
					var csvval=e.target.result.split("\n");
					var csvvalue=csvval[0].split(",");
					for (var i = 1; i < csvval.length; i++) {
						var data = csvval[i].split(',');
						if (data.length == csvvalue.length) {
							var tarr = {};
							for (var j = 0; j < csvvalue.length; j++) {
								tarr[csvvalue[j]] =  data[j];
							}
							lines.push(tarr);
						}
					}					
					var columnnames = new Array();
					for(var i=0;i<csvvalue.length;i++) {
						var temp=csvvalue[i];                       
						columnnames.push(temp);
					} 
					$('.student-import-fields').find('option:not(:first)').remove(); //Remove Previously added Option									
					$.each(columnnames,function(key,value){
						$('.student-import-fields').append($('<option></option>', { value: value,text : value }));
					});
					$('.mapsection').show();					
				};
				reader.readAsText(e.target.files.item(0));
			}
			
			return false;
		});
		
		$('#StudentImportForm').submit(function(e){
			e.preventDefault();
		});
		
		$("#StudentImportForm").validate({
			rules: {
				s_user_name: "required",	
				s_email: "required",									
				s_fname: "required",				
							
				s_lname: "required",								
				p_user_name: "required",				
				p_email: "required",									
				p_fname: "required",						
				p_lname: "required",
			},
			messages: {
				s_user_name: { required: "Please Select Student Login Name Column" },
				s_email: { required: "Please Select Student Email ID Column" },
				s_fname: { required: "Please Select Student First Name Column" },				
				
				s_lname: { required: "Please Select Student Last Name Column" },				
				p_user_name: { required: "Please Select Parent Login Name Column" },				
				p_email: { required: "Please Select Parent Email ID Column" },				
				p_fname: { required: "Please Select Parent First Name Column" },				
				p_lname: { required: "Please Select Parent Last Name Column" }
			},
			submitHandler: function(form){
					$('.pnloader').remove();
					
					if($('#ImpClassID').val()==''){
						$.fn.notify('error',{'desc':'Select class!'});
						return false;
					}
					
					if( $('#s_email').val()== $('#p_email').val() ) {
						$.fn.notify('error',{'desc':'Student and Parent Email ID Shouldn\'t be same'});
						return false;
					}					
					
					if( $('#s_user_name').val()== $('#p_user_name').val() ) {
						$.fn.notify('error',{'desc':'Student and Parent User Name Shouldn\'t be same'});
						return false;
					}
						
					var formdata = $('#StudentImportForm').serializeArray();
					var details=$('#ImportDetails').serializeArray();					
					/*Check Duplicate Values*/
					var results = [];
					jQuery.each( formdata, function( i, field ) {
					 results.push( field.value + " " );
					});
					var mapArray = results.sort();
					var mapValueDuplicate = [];
					
					for (var i = 0; i < mapArray.length - 1; i++) {
						if (mapArray[i] != " " && mapArray[i] != "") {
							mapValueDuplicate.push(mapArray[i]);
						}
						else{
							console.log('maparray else',mapArray);
						}
					}
						
					$.fn.notify('loader',{'desc':'Importing Students'});
					
					var action = "ImportUser";										
					var data = new FormData();					
					
					var formdata = $('#StudentImportForm').serializeArray();
					var details=$('#ImportDetails').serializeArray();
					
					data.append('action', 'ImportUser');					
					var ufile = $('#importcsv')[0].files[0];					
					data.append('importcsv', ufile);	
					
					data.append('uservalues', JSON.stringify( formdata) );
					data.append('details', JSON.stringify( details));
					console.log(data);
					
					$.ajax({
						type: "POST",
						url: ajax_url,
						data: data,
						cache: false,
						processData: false,
						contentType: false,						
						success: function(rdata) {
							$('.pnloader').remove();
							$.fn.notify('success',{'desc':rdata});	
							$('#ImportModal').modal('hide');
						}
					});
				}
		});
});