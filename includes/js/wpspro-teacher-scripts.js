jQuery( document ).ready( function( $ ) {	
	$('#PrintTeacher').click(function() {	
		var columns=$('#TeacherColumnForm').serializeArray();
		columns.push({name: 'action', value: 'TeachersPrint'});
		jQuery.post(ajax_url, columns, function(pdata) {
			var HTML = pdata;
			var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
			WindowObject.document.writeln(HTML);
			WindowObject.document.close();
			WindowObject.focus();
			WindowObject.print();
			WindowObject.close();
		});
	});	
	$('#ExportTeachers').click(function() {
		var columns=$('#ExportColumnForm').serializeArray();
		columns.push({name: 'action', value: 'exportteacher'}); 
		$.ajax({
						type: "POST",
						url: ajax_url,
						data: columns,
						cache: false,
						processData: true,
						success: function(rdata) {
							var mainarrayvalue=[];
							 var titleheade = "";
							jQuery.each( jQuery.parseJSON(rdata), function( i, val ) {
							var subarray=[];
							var j=0;							
								jQuery.each( val, function( f, val1 ) {
									
									subarray[j]=val1;
									if(i ==0){
									 titleheade = titleheade + ','+f;
									}
									j++;
									});
									mainarrayvalue[i]=subarray;
										});

								var titleheademain = titleheade.substr(1);
									titleheademain=titleheademain+'\n'; 
 
 

							mainarrayvalue.forEach(function(row) {
								titleheademain += row.join(',');
								titleheademain += "\n";
								});
 
								var hiddenElement = document.createElement('a');
								hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(titleheademain);
								hiddenElement.target = '_blank';
								hiddenElement.download = 'ExportTeacher.csv';
								hiddenElement.click();
						}
					});
	});	
	/*$('#ImportTeacher').on('click',function(e){
		e.preventDefault();
		$('#ImportModal').modal('show');
	});*/
	$('#importcsv').change(function(e){
		var lines = [];
		var ext = $("input#importcsv").val().split(".").pop().toLowerCase();
		if($.inArray(ext, ["csv"]) == -1) {
			$.fn.notify('error',{'desc':'File format must be CSV!'});
			return false;
		}
		if ( e.target.files != undefined ) {
			var reader = new FileReader();
			reader.onload = function(e) {
				var csvval=e.target.result.split("\n");
				var csvvalue=csvval[0].split(",");
				for (var i = 1; i < csvval.length; i++) {
					var data = csvval[i].split(',');
					if (data.length == csvvalue.length) {
						var tarr = {};
						for (var j = 0; j < csvvalue.length; j++) {
							tarr[csvvalue[j]] =  data[j];
						}
						lines.push(tarr);
					}
				}				
				var columnnames = new Array();
				for(var i=0;i<csvvalue.length;i++) {
					var temp=csvvalue[i];
					columnnames.push(temp);
				}	
				$('.teacher-import-fields').find('option:not(:first)').remove(); //Remove Previously added Option				
				$.each(columnnames,function(key,value){
				   $('.teacher-import-fields').append($('<option></option>', { value: value,text : value }));
				});
				$('.mapsection').show();
			};
			reader.readAsText(e.target.files.item(0));		}
		return false;
	});		
	$("#TeacherImportForm").validate({
			rules: {
				'first_name':'required',
				
				'last_name':'required',				
				'user_email':'required',				
				'user_login':'required',				
				'user_password':'required',
			},
			messages: {
			},
			submitHandler: function(form){
				var formdata = $('#TeacherImportForm').serializeArray();
				var details=$('#ImportDetails').serializeArray();
				/*Check Duplicate Values*/
				var results = [];
				jQuery.each( formdata, function( i, field ) {
					results.push( field.value + " " );
				});				
				var mapArray = results.sort(); 
				var mapValueDuplicate = [];
				
				for (var i = 0; i < mapArray.length - 1; i++) {
						
						if (mapArray[i] != " " && mapArray[i] != "") {
							mapValueDuplicate.push(mapArray[i]);	
						}
						else{
							console.log('maparray else',mapArray);
						}
					}
				for (var i = 0; i < mapArray.length - 1; i++) {
					if (mapArray[i + 1] == mapArray[i]) {
						mapValueDuplicate.push(mapArray[i]);
					}
				}
			
				$.fn.notify('loader',{'desc':'Importing Teachers'});										
				var data = new FormData();									
				var fdata = $('#ImportDetails').serializeArray();									
				data.append('uservalues', JSON.stringify( formdata) );
				data.append('details', JSON.stringify( details));
				data.append('action', 'ImportTeacher');									
				var ufile = $('#importcsv')[0].files[0];									
				data.append('importcsv', ufile);
					
				$.ajax({
					type: "POST",
					url: ajax_url,
					data: data,
					cache: false,
					processData: false,
					contentType: false,					
					success: function(rdata) {
						$('.pnloader').remove();
						$.fn.notify('success',{'desc':rdata});
						$('#ImportModal').modal('hide');
						location.reload();
					}
				});
			}
	});
});