<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Public Pages Class
 * 
 * Handles all the different features of student module
 * for the front end pages.
 * 
 * @package 
 * @since 1.0.0
 */
class Wpspro_Student {
	function wpspro_export_student() {
		if( isset( $_POST['exportstudent'] ) &&  $_POST['exportstudent']=='exportstudent' && isset( $_POST['ClassID'] )) {
			$response		=	$this->wpspro_get_student_data();
			$result			=	$response['result'];
			$exportcolumn	=	$response['exportcolumn'];
			$finalarray 	= 	json_encode($result);
			$csv_header 	= 	'';
			$csv_header    .= 	implode( ",", $exportcolumn );
			$csv_header	   .= 	"\n";
			$csv_row ='';
			foreach($finalarray as $line ){
				if( is_array( $line ) ) {
					foreach( $line as $key=>$value ) {
						$csv_row .= '"' . $value . '",';
					}
				} else {
					$csv_row .= '"' . $line . '",';
				}
				$csv_row .= "\n";
			}		
			/* Download as CSV File */
			print_r( $finalarray);
			
			exit;
		}
	}	
	function wpspro_get_student_data() {
		global $wpdb;
		$user_table		=	$wpdb->prefix."users";
		$student_table	=	$wpdb->prefix."wpsp_student";
		$parent_table	=	$wpdb->prefix."wpsp_parent";
		$class_table	=	$wpdb->prefix."wpsp_class";
		$names 			= 	array(	's_rollno'			=>	__('Roll Number', 'WPSchoolPress'),
									's_fname'			=>	__('Student First Name', 'WPSchoolPress'),	
									's_mname'			=>	__('Student Middle Name', 'WPSchoolPress'),	
									's_lname'			=>	__('Student Last Name', 'WPSchoolPress'),	
									's_zipcode'			=>	__('Zip Code', 'WPSchoolPress'),	
									's_country'			=>	__('Country', 'WPSchoolPress'),	
									's_gender'			=>	__('Gender', 'WPSchoolPress'),	
									's_address'			=>	__('Current Address', 'WPSchoolPress'),	
									's_paddress'		=>	__('Permanent Address', 'WPSchoolPress'),	
									'p_fname '			=>	__('Parent First Name', 'WPSchoolPress'),	
									's_bloodgrp'		=>	__('Blood Group', 'WPSchoolPress'),	
									's_dob'				=>	__('Date Of Birth', 'WPSchoolPress'),	
									's_doj'				=>	__('Date Of Join', 'WPSchoolPress'),										
									's_phone'			=>	__('Phone Number', 'WPSchoolPress'),	
							);
		$cid			=	$_POST['ClassID'];
		$data			=	$_POST['StudentColumn'];
		$condition		=	$exportcolumn	=	array();
		foreach($data as $value){
			$exportcolumn[] = $names[$value];	
			$condition[] = $value;
			
		}
		$conditionimplode = implode(',',$condition);	
		$classWhere	=	'';	
		if( $cid != '0' && $cid != '' )
			$classWhere	=	"where class_id='$cid'";
		$result = $wpdb->get_results("select $conditionimplode from $student_table $classWhere order by s_rollno ASC");
		$response['result'] 		= $result;
		$response['exportcolumn'] 	= $exportcolumn;
		return $response;
	}	
	

	function wpspro_student_scripts() {
		if ( is_page( 'sch-student' ) ) {			
			echo "<link href='".WPSPRO_PLUGIN_URL."includes/css/wpsp-code-public.css' rel='stylesheet' type='text/css' media='print'/>";
			echo "<script src='".WPSPRO_PLUGIN_URL . "includes/js/wpspro-student-scripts.js'></script>";
		}		
	}	
	function wpspro_student_print() {
		$response		=	$this->wpspro_get_student_data();

		$result			=	$response['result'];
		$exportcolumn	=	$response['exportcolumn'];
		$finalarray 	= 	json_decode(json_encode($result), True);

		if(count($finalarray)){
			echo "<html>";
			echo "<head><title>Students Lists</title> 
			<style>
			*{margin:0; padding:0;}	
			table{width:100%;border-collapse:separate;clear: both;margin: 6px 0px!important;border:1px solid #e2e2e2; border-right:none; border-bottom:none;}
			table tr th{background-color:#f5f5f5; font-size:12px; font-weight: bold; color:#000;border-right:1px solid #e2e2e2; border-bottom:1px solid #e2e2e2; padding:5px; }
			table tr:nth-of-type(odd) {background-color: #f9f9f9;}	
			table tr td{border-right:1px solid #e2e2e2; border-bottom:1px solid #e2e2e2; font-size:14px; color:#000; padding:5px;}

</style>";
			
			echo "</head><body>";
			echo "<table cellspacing='0' cellpadding='0' border='0' style='margin:0; padding:0;'>";
			foreach($exportcolumn as $header){
				echo "<th bgcolor='#f5f5f5' style='background-color:#f5f5f5'>".$header."</th>";
			}
			foreach($finalarray as $row){
				echo "<tr>";
				foreach($row as $column){
					echo "<td>".$column."</td>";
				}
				echo "</tr>";
			}
			echo "</table></body></html>";
		}
		exit();
	}
	
	function wpspro_student_import_html() { 
		ob_start();
	?>		<!-- Modal for Import-->
	<div class="wpsp-popupMain" id="ImportModal">
			  <div class="wpsp-overlayer"></div> 
			  <div class="wpsp-popBody"> 
				<div class="wpsp-popInner">
					<a href="javascript:;" class="wpsp-closePopup"></a>
					
						<div class="wpsp-panel-heading">
								<h3 class="wpsp-panel-title"><?php _e( 'Import Students', 'WPSchoolPress'); ?></h3>
						</div>
					
					<div class="wpsp-col-md-12">
						<div class="box box-info">
													
							<div class="wpsp-col-md-6">
								<form action="#" name="ImportDetails" id="ImportDetails">
									<div class="wpsp-form-group">
										<label class="wpsp-label" for="class"><?php _e( 'Select Class', 'WPSchoolPress'); ?> </label>
										<?php $classes=wpsp_ClassList(); ?>
										<?php wp_nonce_field( 'UserImport', 'import_nonce', '', true ) ?>
										<input type="hidden" name="userType" value="1">
										<select name="ClassID" id="ImpClassID" class="wpsp-form-control">
											<option value="">Select Class</option>
											<?php foreach($classes as $class){?>
												<option value="<?php echo $class['cid'];?>"><?php echo $class['c_name'];?></option>
											<?php } ?>
										</select>
									</div>
								</form>
							</div>
							<form name="StudentImportForm" id="StudentImportForm" method="post" enctype="multipart/form-data">
								<div class="box-body">
									<div class="wpsp-col-md-4">
										<div class="wpsp-form-group">
											<label for="importcsv">Attach CSV file</label>
											<input type="file" name="importcsv" id="importcsv">	
											<a href="<?php echo WPSPRO_PLUGIN_URL;?>includes/sample/StudentImportSample.csv">Download Sample CSV </a>
										</div>
									</div>
									<div class="wpsp-col-md-12">
										<ul class="italic">
											<li>File must be comma delimited CSV file</li>
											<li>Open excel save as comma delimited CSV file</li>
											<li>Make sure there is no heading or anyother content than column header and values in CSV file</li>
											<li>You can undo this operation anytime from import history</li>
										</ul>
									</div>
									<div class=" mapsection" style="display:none;">
										<?php
											 $studentFieldList =  array( 's_fname'		=>	__('First Name', 'WPSchoolPress'),
																		's_mname'		=>	__('Middle Name', 'WPSchoolPress'),
																		's_lname'		=>	__('Last Name', 'WPSchoolPress'),
																		's_email'		=>	__('Email ID', 'WPSchoolPress'),
																		's_user_name'	=>	__('Login Name', 'WPSchoolPress'),
																		's_user_pass'	=>	__('Login Password', 'WPSchoolPress'),
																		's_dob'			=>	__('Date Of Birth', 'WPSchoolPress'),
																		's_gender'		=>	__('Gender', 'WPSchoolPress'),																		
																		's_address'		=>	__('Address', 'WPSchoolPress'),	
																		's_city'		=>	__('City', 'WPSchoolPress'),
																		's_country'		=>	__('Country', 'WPSchoolPress'),	
																		's_zipcode'		=>	__('PinCode', 'WPSchoolPress'),
																		's_phone'		=>	__('Phone Number', 'WPSchoolPress'),
																		's_bloodgrp'	=>	__('Blood Group', 'WPSchoolPress'),																		
																		's_doj'			=>	__('Date Of Join', 'WPSchoolPress'),
																		's_rollno'		=>	__('Roll Number', 'WPSchoolPress'),
																		'p_email'		=>	__('Parent Email ID', 'WPSchoolPress'),	
																		'p_fname'		=>	__('Parent First Name', 'WPSchoolPress'),
																		'p_mname'		=>	__('Parent Middle Name', 'WPSchoolPress'),
																		'p_lname'		=>	__('Parent Last Name', 'WPSchoolPress'),																		
																		'p_user_name'	=>	__('Parent Login Name', 'WPSchoolPress'),	
																		'p_user_pass'	=>	__('Parent Login Password', 'WPSchoolPress'),
																		'p_gender'		=>	__('Parent Gender', 'WPSchoolPress'),
																		'p_edu'			=>	__('Parent Education', 'WPSchoolPress'),
																		'p_profession'	=>	__('Parent Profession', 'WPSchoolPress'),
																		'p_bloodgrp'	=>	__('Parent BloodGroup', 'WPSchoolPress')																		
																);
											$requiredfields		=	array( 's_user_name', 's_email', 's_fname', 's_lname', 's_user_pass', 'p_user_name', 'p_email', 'p_fname', 'p_lname', 'p_user_pass'  );		
											$counter = intval ( count( $studentFieldList ) / 2 );
											$i=0;
											foreach( $studentFieldList as $key=>$value ) {
										?>
												<div class="wpsp-col-md-4 ">
													<div class="wpsp-form-field" style="margin-bottom: 15px;">
													<label><?php echo $value; ?></label>
													<select class="wpsp-form-control student-import-fields" name="<?php echo $key; ?>" id="<?php echo $key; ?>" <?php if( in_array( $key, $requiredfields ) ) { ?> required <?php } ?>>
														<option value="">Select <?php echo $value; ?> column</option>
													</select>
													</div>
												</div>
										<?php
											$i++;										}
										?>
									</div>
								</div>
								<div class="box-footer mapsection" style="display:none;">														
									<button type="submit" id="StudentImport" class="wpsp-btn wpsp-btn-primary">Submit</button>
								</div>
							</form>
							</div>
					</div>
				
				</div>
			</div>
		</div><!-- /.modal -->
	<?php
		$html = ob_get_clean();
		echo $html;
	}	
	public function add_hooks() {		
		add_action( 'wp_ajax_exportstudent', array( $this, 'wpspro_export_student' ) );
		add_action( 'wpsp_footer_script', array( $this, 'wpspro_student_scripts' ) );
		add_action( 'wp_ajax_StudentsPrint', array( $this, 'wpspro_student_print' ) );		
		add_action( 'wpsp_student_import_html', array( $this, 'wpspro_student_import_html'));
	}
}