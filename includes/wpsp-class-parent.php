<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Public Pages Class
 * 
 * Handles all the different features of parent module
 * for the front end pages. * 
 * @package 
 * @since 1.0.0
 */
class Wpspro_Parent {	
	function wpspro_export_parent() {		
		if( isset( $_POST['exportparent'] ) &&  $_POST['exportparent']=='exportparent' && isset( $_POST['ClassID'] ) && isset( $_POST['ParentColumn'] ) && count( $_POST['ParentColumn'] ) > 0 ) {	
			$response		=	$this->wpspro_get_parent_data();
			$result			=	$response['result'];
			$exportcolumn	=	$response['exportcolumn'];
			$finalarray 	= 	json_encode($result);
			$csv_header 	= 	'';
			$csv_header    .= 	implode( ",", $exportcolumn );
			$csv_header	   .= 	"\n";
			$csv_row ='';		
			foreach($finalarray as $line ) {
				if( is_array( $line ) && !empty( $line) ) {
					foreach( $line as $key=>$value ) {
						$csv_row .= '"' . $value . '",';
					}
				} else {
					$csv_row .= '"' . $line . '",';
				}
				$csv_row .= "\n";
			}		
			
			print_r( $finalarray);
			exit;
		}
	}
	
	function wpspro_get_parent_data() {	
		global $current_user, $wp_roles, $wpdb;
		$user_table		=	$wpdb->prefix."users";
		$student_table	=	$wpdb->prefix."wpsp_student";
		$parent_table	=	$wpdb->prefix."wpsp_parent";
		$class_table	=	$wpdb->prefix."wpsp_class";
		$names 			= 	array(	'p_fname'		=>	__('First Name', 'WPSchoolPress'),	
									'p_mname'		=>	__('Middle Name', 'WPSchoolPress'),	
									'p_lname'		=>	__('Last Name', 'WPSchoolPress'),
									's_fname'		=>	__('Student Name', 'WPSchoolPress'),
									'user_email'	=>	__('Parent Email ID', 'WPSchoolPress'),
									'p_edu'			=>	__('Education', 'WPSchoolPress'),									
									'p_gender'		=>	__('Gender', 'WPSchoolPress'),
									'p_profession'	=>	__('Profession', 'WPSchoolPress'),
									'p_bloodgrp'	=>	__('Blood Group', 'WPSchoolPress'),
							);
		$cid			=	$_POST['ClassID'];
		$data			=	$_POST['ParentColumn'];
		$condition 		= 	$exportcolumn	=	array();		
		foreach($data as $value){
			$exportcolumn[] = $names[$value];
			if(($value == 'user_email') || ($value == 'user_login') || ($value == 'display_name') ) {
				$condition[] = 'u.'.$value;
			} else {
				$condition[] = 's.'.$value;
			}
		}		
		$conditionimplode = implode(',',$condition);
		$classQuery	= '';
		
		if( $cid != '0' && $cid != '' )
			$classQuery	= " where s.class_id='$cid'";
		
		$result = $wpdb->get_results("select $conditionimplode from $student_table s left join  $user_table u on u.ID=s.parent_wp_usr_id $classQuery");
		$response['result'] 		= $result;
		$response['exportcolumn'] 	= $exportcolumn;
		return $response;
	}
	
	function wpspro_parent_scripts() {		
		if ( is_page( 'sch-parent' ) ) {
			echo "<script src='".WPSPRO_PLUGIN_URL . "includes/js/wpspro-parent-scripts.js'></script>";
		}
	}
	
	function wpspro_parent_print() {
		$response		=	$this->wpspro_get_parent_data();
		$result			=	$response['result'];
		$exportcolumn	=	$response['exportcolumn'];
		$finalarray 	= 	json_decode(json_encode($result), True);
		if(count($finalarray)){
			
			echo "<html>";
			echo "<head><title>Students Lists</title> 
			<style>
			*{margin:0; padding:0;}	
			table{width:100%;border-collapse:separate;clear: both;margin: 6px 0px!important;border:1px solid #e2e2e2; border-right:none; border-bottom:none;}
			table tr th{background-color:#f5f5f5; font-size:12px; font-weight: bold; color:#000;border-right:1px solid #e2e2e2; border-bottom:1px solid #e2e2e2; padding:5px; }
			table tr:nth-of-type(odd) {background-color: #f9f9f9;}	
			table tr td{border-right:1px solid #e2e2e2; border-bottom:1px solid #e2e2e2; font-size:14px; color:#000; padding:5px;}

</style>";
			//echo "<link href='".WPSPRO_PLUGIN_URL."includes/css/wpsp-print.css' rel='stylesheet' type='text/css' />";
			echo "</head><body>";
			echo "<table>";
			foreach($exportcolumn as $header){
				echo "<th>".$header."</th>";
			}			
			foreach($finalarray as $row){
				echo "<tr>";
				foreach($row as $column){
					echo "<td>".$column."</td>";
				}
				echo "</tr>";
			}
			echo "</table></body></html>";
		}
		exit();
	}
	
	function wpspro_parent_import_html() { ?>
		<div class="wpsp-popupMain" id="ImportModal">
			<div class="wpsp-overlayer"></div> 
			<div class="wpsp-popBody"> 
				<div class="wpsp-popInner">
					<a href="javascript:;" class="wpsp-closePopup"></a>
						<div class="wpsp-panel-heading">
								<h3 class="wpsp-panel-title"><?php _e( 'Import parents', 'WPSchoolPress'); ?></h3>
						</div>
						<div class="wpsp-panel-body">
							<div class="wpsp-col-md-12">
								<div class="box box-info">
									<div class="wpsp-col-md-6">
										<form action="#" name="ImportDetails" id="ImportDetails">
											<div class="wpsp-form-group">
												<label class="wpsp-label" for="class">
													<?php _e( 'Select Class', 'WPSchoolPress'); ?>
												</label>
												<?php $classes=wpsp_ClassList(); ?>
												<?php wp_nonce_field( 'UserImport', 'import_nonce', '', true ) ?>
												<input type="hidden" name="userType" value="1">
												<select name="ClassID" id="ImpClassID" class="form-control">
													<option value="">Select Class</option>
													<?php foreach($classes as $class){?>
														<option value="<?php echo $class['cid'];?>">
														<?php echo $class['c_name'];?></option>
													<?php } ?>
												</select>
											</div>
										</form>
									</div>
									<form name="parentImportForm" id="parentImportForm" method="post" enctype="multipart/form-data">
										<div class="box-body">
											<div class="wpsp-col-md-4">
												<div class="wpsp-form-group">
													<label for="importcsv">Attach CSV file</label>
													<input type="file" name="importcsv" id="importcsv">
													<a href="<?php echo WPSPRO_PLUGIN_URL;?>includes/sample/parentImportSample.csv">Download Sample CSV </a>
												</div>
											</div>
											<div class="wpsp-col-md-8">
												<ul class="italic">
													<li>File must be comma delimited CSV file</li>
													<li>Open excel save as comma delimited CSV file</li>
													<li>Make sure there is no heading or anyother content than column header and values in CSV file</li>
													<li>You can undo this operation anytime from import history</li>
												</ul>
											</div>
											<div class="wpsp-col-md-12 mapsection" style="display:none;">	
											<?php
											$parentFieldList =  array(	'rollno'		=>	__('Roll Number', 'WPSchoolPress'),
																	'user_name'			=>	__('User Name', 'WPSchoolPress'),	
																	'user_email'		=>	__('User Email', 'WPSchoolPress'),
																	'user_pass'			=>	__('User Password', 'WPSchoolPress'),
																	'first_name'		=>	__('First Name', 'WPSchoolPress'),
																	'middle_name'		=>	__('Middle Name', 'WPSchoolPress'),
																	'last_name'			=>	__('Last Name', 'WPSchoolPress'),	
																	'zipcode'			=>	__('Zip Code', 'WPSchoolPress'),
																	'country'			=>	__('Country', 'WPSchoolPress'),			
																	'gender'			=>	__('Gender', 'WPSchoolPress'),			
																	'address'			=>	__('Address', 'WPSchoolPress'),	
																	//'parent_wp_usr_id'	=>	__('Parent Name', 'WPSchoolPress'),
																	'bloodgrp'			=>	__('Blood Group', 'WPSchoolPress'),					
																	'dob'				=>	__('Date Of Birth', 'WPSchoolPress'),			
																	'doj'				=>	__('Date Of Join', 'WPSchoolPress'),		
																	'phone'				=>	__('Phone Number', 'WPSchoolPress') );
														$counter = intval ( count( $parentFieldList ) / 2 );							
														$i=0;
														foreach( $parentFieldList as $key=>$value ) {
															if( $i==0 || $i==$counter )
																echo '<div class="col-md-6">';
														?>								
														<div class="wpsp-form-group">
															<label><?php echo $value; ?></label>
															<select class="wpsp-form-control parent-import-fields" name="<?php echo $key; ?>" id="<?php echo $key; ?>">
																<option value="">Select <?php echo $value; ?> column</option>										
															</select>
														</div>
														<?php
																if( $i == ( $counter-1 ) ||  $i == ( count( $parentFieldList ) ) )
																	echo '</div>';
																$i++;
															}
														?>	
											</div>
										</div>
										<div class="box-footer mapsection" style="display:none;">
											<button type="submit" id="parentImport" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					
				</div>
			</div>
		</div>
		<!-- Modal for Import-->
		<?php
	}
	
	public function add_hooks() {		
		// Export parent
		add_action( 'wp_ajax_exportparent', array( $this, 'wpspro_export_parent' ) );
		add_action( 'wpsp_footer_script', array( $this, 'wpspro_parent_scripts' ) );
		add_action( 'wp_ajax_ParentPrint', array( $this, 'wpspro_parent_print' ) );
		add_action( 'wpsp_parent_import_html', array( $this, 'wpspro_parent_import_html'));
	}
}