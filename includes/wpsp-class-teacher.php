<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * teacher Pages Class
 * 
 * Handles all the different features of teacher module
 * for the front end pages.
 * 
 * @package 
 * @since 1.0.0
 */
class Wpspro_Teacher {	
	function wpspro_export_teacher() {
		if( isset( $_POST['exportteacher'] ) &&  $_POST['exportteacher']=='exportteacher' && isset( $_POST['TeacherColumn'] ) && count( $_POST['TeacherColumn'] ) > 0 ) {
			$response		=	$this->wpspro_get_teacher_data();
			$result			=	$response['result'];
			$exportcolumn	=	$response['exportcolumn'];
			$finalarray 	= 	json_encode($result);
			$csv_header 	= 	$csv_row	=	'';
			$csv_header    .= 	implode( ",", $exportcolumn );
			$csv_header	   .= 	"\n";			
			foreach($finalarray as $line ){
				if( is_array( $line ) ) {
					foreach( $line as $key=>$value ) {
						$csv_row .= '"' . $value . '",';
					}
				} else {
					$csv_row .= '"' . $line . '",';
				}
				$csv_row .= "\n";
			}	
			/* Download as CSV File */
			print_r( $finalarray);
			exit;
		}
	}
	function wpspro_get_teacher_data() {	
		global $current_user, $wp_roles, $wpdb;
		$user_table		=	$wpdb->prefix."users";
		$teacher_table	=	$wpdb->prefix."wpsp_teacher";
		$class_table	=	$wpdb->prefix."wpsp_class";		$subjects_table =	$wpdb->prefix."wpsp_subject";
		$condition 		=	$exportcolumn	=	array();
		$data			=	$_POST['TeacherColumn'];		$classquery		=	$teacherQuery	=	'';		
		$names 			=	array(	'empcode '			=>	__('Emp. Code', 'WPSchoolPress'),									'first_name'		=>	__('First Name', 'WPSchoolPress'),										'middle_name'		=>	__('Middle Name', 'WPSchoolPress'),										'last_name'			=>	__('Last Name', 'WPSchoolPress'),									'user_email'		=>	__('Teacher Email', 'WPSchoolPress'),									'zipcode'			=>	__('Zip Code', 'WPSchoolPress'),										'country'			=>	__('Country', 'WPSchoolPress'),									'gender'			=>	__('Gender', 'WPSchoolPress'),									'address'			=>	__('Address', 'WPSchoolPress'),																			'dob'				=>	__('Date Of Birth', 'WPSchoolPress'),									'doj'				=>	__('Date Of Join', 'WPSchoolPress'),										'dol'				=>	__('Date Of Releaving', 'WPSchoolPress'),									'phone'				=>	__('Phone Number', 'WPSchoolPress'),									'qualification'	    =>	__('Qualification', 'WPSchoolPress'),									'gender'			=>	__('Gender', 'WPSchoolPress'),									'bloodgrp'			=>	__('Blood Group', 'WPSchoolPress'),									'position'			=>	__('Position', 'WPSchoolPress'),									'whours'			=>	__('Working Hours', 'WPSchoolPress'),							);	 
		foreach($data as $value){
			$exportcolumn[] = $names[$value];
			if(($value == 'user_email') || ($value == 'user_login') || ($value == 'display_name')){
				$condition[] = 'u.'.$value;
			}else{
				$condition[] = 't.'.$value;
			}
		}		$sel_classid	=	isset( $_POST['classid'] ) ? $_POST['classid'] : '';
		if( !empty( $sel_classid ) && $sel_classid!='all' ){			$classquery	=	" AND c.cid=$sel_classid ";		}		$sub_han		=	$wpdb->get_results("select sub_name,sub_teach_id,c.c_name from $subjects_table s, $class_table c where sub_teach_id>0 AND c.cid=s.class_id $classquery order by c.cid");		foreach($sub_han as $subhan) {			$sub_handling[$subhan->sub_teach_id][]=$subhan->sub_name.' ('.$subhan->c_name.')';			$teacher[]	=	$subhan->sub_teach_id;		}		$incharges=$wpdb->get_results("select c.c_name,c.teacher_id from $class_table c LEFT JOIN $teacher_table t ON t.wp_usr_id=c.teacher_id where c.teacher_id>0 $classquery");		foreach($incharges as $incharge){			$cincharge[$incharge->teacher_id][]=$incharge->c_name;			}		if( !empty( $teacher ) && !empty( $sel_classid ) && $sel_classid!='all' ) {			$teacherQuery	=	' WHERE wp_usr_id IN ('.implode( ", " , $teacher ).") ";		}		
		$conditionimplode = implode(',',$condition);		
		$result = $wpdb->get_results("select $conditionimplode from $teacher_table t left join $user_table u on u.ID = t.wp_usr_id $teacherQuery order by t.tid DESC");
		$response['result'] 		= $result;
		$response['exportcolumn'] 	= $exportcolumn;
		return $response;
	}	
	function wpspro_teacher_scripts() {
		if ( is_page( 'sch-teacher' ) ) {
			echo "<script src='".WPSPRO_PLUGIN_URL . "includes/js/wpspro-teacher-scripts.js'></script>";
		}
	}	
	function wpspro_teacher_prints() {
		$response		=	$this->wpspro_get_teacher_data();
		$result			=	$response['result'];
		$exportcolumn	=	$response['exportcolumn'];
		$finalarray 	= 	json_decode(json_encode($result), True);
		if(count($finalarray)){
			echo "<html>";
			echo "<head><title>Students Lists</title> 
			<style>
			*{margin:0; padding:0;}	
			table{width:100%;border-collapse:separate;clear: both;margin: 6px 0px!important;border:1px solid #e2e2e2; border-right:none; border-bottom:none;}
			table tr th{background-color:#f5f5f5; font-size:12px; font-weight: bold; color:#000;border-right:1px solid #e2e2e2; border-bottom:1px solid #e2e2e2; padding:5px; }
			table tr:nth-of-type(odd) {background-color: #f9f9f9;}	
			table tr td{border-right:1px solid #e2e2e2; border-bottom:1px solid #e2e2e2; font-size:14px; color:#000; padding:5px;}

</style>";
			echo "</head><body>";
			echo "<table>";
			foreach($exportcolumn as $header){
				echo "<th>".$header."</th>";
			}
			foreach($finalarray as $row){
				echo "<tr>";
				foreach($row as $column){
					echo "<td>".$column."</td>";
				}
				echo "</tr>";
			}
			echo "</table></body></html>";
		}
		exit();
	}
	function wpspro_teacher_import_html() {
		ob_start();
	?>		
	<div class="wpsp-popupMain" id="ImportModal" >
		<div class="wpsp-overlayer"></div> 
		<div class="wpsp-popBody"> 
			<div class="wpsp-popInner">
				<a href="javascript:;" class="wpsp-closePopup"></a>
				<div id="ViewModalContent">
					<div class="wpsp-panel-heading">
						<h3 class="wpsp-panel-title">Import Teacher</h3>
					</div>
					<div class="wpsp-panel-body">
						<div class="wpsp-col-md-12">
							<form action="#" name="ImportDetails" id="ImportDetails">
								<div class="wpsp-form-group">
									<?php wp_nonce_field( 'UserImport', 'import_nonce', '', true ) ?>
									<input type="hidden" name="userType" value="3">
								</div>
							</form>							
							<form name="TeacherImportForm" id="TeacherImportForm" method="post" enctype="multipart/form-data">
								<div class="wpsp-panel-body">
									<div class="wpsp-col-md-4">
										<div class="wpsp-form-group">
											<label class="wpsp-label" for="importcsv">Attach CSV file</label>
											<input type="file" name="importcsv" id="importcsv">												<a href="<?php echo WPSPRO_PLUGIN_URL;?>includes/sample/TeacherImportSample.csv">Download Sample CSV </a>
										</div>
									</div>
									<div class="wpsp-col-md-8">
										<ul class="italic">
											<li>File must be comma delimited CSV file</li>
											<li>Open excel save as comma delimited CSV file</li>
											<li>Make sure there is no heading or anyother content than column header and values in CSV file</li>
											<li>You can undo this operation anytime from import history</li>
										</ul>
									</div>
									<div class="wpsp-col-md-12 mapsection" style="display:none;">
									<?php
										$teacherFieldList =  array(	'empcode '			=>	__('Emp. Code', 'WPSchoolPress'),																	'first_name'		=>	__('First Name', 'WPSchoolPress'),																		'middle_name'		=>	__('Middle Name', 'WPSchoolPress'),																		'last_name'			=>	__('Last Name', 'WPSchoolPress'),																	'user_email'		=>	__('Email', 'WPSchoolPress'),																	'user_login'		=>	__('Teacher Login Name', 'WPSchoolPress'),																	'user_password'		=>	__('Password', 'WPSchoolPress'),																	'zipcode'			=>	__('Zip Code', 'WPSchoolPress'),																		'country'			=>	__('Country', 'WPSchoolPress'),																	'city'				=>	__('city', 'WPSchoolPress'),																	'gender'			=>	__('Gender', 'WPSchoolPress'),																	'address'			=>	__('Address', 'WPSchoolPress'),																											'dob'				=>	__('Date Of Birth', 'WPSchoolPress'),																	'doj'				=>	__('Date Of Join', 'WPSchoolPress'),																		'dol'				=>	__('Date Of Releaving', 'WPSchoolPress'),																	'phone'				=>	__('Phone Number', 'WPSchoolPress'),																	'qualification'	    =>	__('Qualification', 'WPSchoolPress'),																	'gender'			=>	__('Gender', 'WPSchoolPress'),																	'bloodgrp'			=>	__('Blood Group', 'WPSchoolPress'),																	'position'			=>	__('Position', 'WPSchoolPress'),																	'whours'			=>	__('Working Hours', 'WPSchoolPress'),															); 
										$counter = intval ( count( $teacherFieldList ) / 2 );																	$requiredfields		=	array( 'first_name','last_name', 'user_email', 'user_login', 'user_password' );		
										$i=0;
										foreach( $teacherFieldList as $key=>$value ) { ?>
											<div class="wpsp-form-group col-md-2">
												<label><?php echo $value; ?></label>
												<select class="wpsp-form-control teacher-import-fields" name="<?php echo $key; ?>" id="<?php echo $key; ?>" <?php if( in_array( $key, $requiredfields ) ) { ?> required <?php } ?>>
													<option value="">Select <?php echo $value; ?> column</option>
												</select>
											</div>
									<?php } ?>
									</div>
								</div>	
								<div class="box-footer mapsection" style="display:none;">
									<button type="submit" id="StudentImport" class="wpsp-btn wpsp-btn-primary">Submit</button>
								</div>
							</form>
						</div>																			
					</div> 
				</div>
			</div>
		</div>	
	</div>
	<?php
		$html = ob_get_clean();
		echo $html;
	}
	
	public function add_hooks() {
		// Export student
		add_action( 'wp_ajax_exportteacher', array( $this, 'wpspro_export_teacher' ) );		
		add_action( 'wpsp_footer_script', array( $this, 'wpspro_teacher_scripts' ) );
		add_action( 'wp_ajax_TeachersPrint', array( $this, 'wpspro_teacher_prints' ) );
		add_action( 'wpsp_teacher_import_html', array( $this, 'wpspro_teacher_import_html'));
	}
}