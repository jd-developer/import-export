<?php
add_action( 'wp_ajax_ImportUser', 'wpsp_ImportUser' );
/*User Import */
function wpsp_ImportUser() {
    wpsp_Authenticate();
	global $wpdb;
	$user_values	= 	json_decode( stripslashes( $_POST['uservalues'] ), true );
	//$csv_values		= 	json_decode( stripslashes( $_POST['allvalues'] ), true );
	$details		=	json_decode( stripslashes( $_POST['details'] ), true );	
	
	$nonce	=	$details[0]['value'];
	$type	=	$details[1]['value'];
	$roles	=	array( 'subscriber', 'student', 'parent', 'teacher' );
	
	if( isset( $details[2] ) )
		$class_id	=	$details[2]['value'];
	
	if (!wp_verify_nonce($nonce, 'UserImport')) 
	{
		echo "Unauthorized Submission";
		exit;
	}
	$finalarray = array();
	$tmpName = $_FILES['importcsv']['tmp_name'];
	$csv_values = array_map('str_getcsv', file($tmpName));
	$csv_mix	=	array();
	
	foreach( $csv_values as $key => $value ) {	
		foreach( $value as $currentkey => $currentvalue ) {
			$mainindex	=	$csv_values[0][$currentkey];			
			$csv_final_values[$key][$mainindex] = $csv_values[$key+1][$currentkey];
		}		
	}
	//$csv_value = array_filter($csv_final_values);
	for( $i=0; $i < count( $csv_final_values ); $i++ ) {
		foreach( $user_values as $key=>$value ) {			
			$finalarray[$i][$value['name']] 	= $csv_final_values[$i][$value['value']];			
		}
	}
	
	
	if( isset( $_POST['Class'] ) && !empty( $_POST['Class'] ) ) {
		$classID	=	$_POST['Class'];
		$capacity	=	$wpdb->get_var("SELECT c_capacity FROM $wpsp_class_table where cid=$classID"); 		
		if( !empty( $capacity ) ) {
			$totalstudent	=	$wpdb->get_var("SELECT count(*) FROM $wpsp_student_table where class_id=$classID");
			if( $totalstudent > count( $finalarray )  ) {
				echo 'This Class reached to it\'s capacity, Please select another class';
				exit;
			}
		}
	}
	
	$error_values	=	$imported	=	array();	
	$success		=	$count	=	0;	
	$upload_dir = wp_upload_dir();
	
	foreach( $finalarray as $sub_ent ) {
		
		if( empty($sub_ent['s_email'] ) || !is_email( $sub_ent['s_email'] ) || empty( $sub_ent['s_fname'] ) || empty( $sub_ent['s_mname'] ) || empty( $sub_ent['s_lname'] ) || empty( $sub_ent['s_user_name'] ) || empty( $sub_ent['s_user_pass'] ) ||
			empty($sub_ent['p_email'] ) || !is_email( $sub_ent['p_email'] ) || empty( $sub_ent['p_fname'] ) || empty( $sub_ent['p_mname'] ) || empty( $sub_ent['p_lname'] ) || empty( $sub_ent['p_user_name'] ) || empty( $sub_ent['p_user_pass'] ) ||
			$sub_ent['s_email']==$sub_ent['p_email'] || $sub_ent['s_user_name']==$sub_ent['p_user_name'] ) 
				continue;
			
		if( is_email( $sub_ent['s_email'] ) ){
			$email=$sub_ent['s_email'];
		}
		
		$userInfo = array(	'user_login' 		=>	esc_attr( $sub_ent['s_user_name'] ),
							'user_pass'			=>	$sub_ent['s_user_pass'],
							'user_nicename'		=>	esc_attr( $sub_ent['s_fname'] ),
							'user_email'		=>	$email,
							'display_name'		=>	esc_attr( $sub_ent['s_fname'] ),
							'user_registered'	=>	date('Y-m-d H:i:s'),
							'role'				=>	'student' );
		$user_id = wp_insert_user( $userInfo );
		
		$parent_register =false;
		$response		=	getparentInfo( $sub_ent['p_email'] ); //check for parent email id

		$pfirstname		=	isset( $sub_ent['p_fname'] ) ? $sub_ent['p_fname'] : '';
		$pmiddlename	=	isset( $sub_ent['p_mname'] ) ? $sub_ent['p_mname'] : '';
		$plastname		=	isset( $sub_ent['p_lname'] ) ? $sub_ent['p_lname'] : '';
		$pgender		=	isset( $sub_ent['p_gender'] ) ? $sub_ent['p_gender'] : '';
		$pedu			=	isset( $sub_ent['p_edu'] ) ? $sub_ent['p_edu'] : '';
		$pprofession	=	isset( $sub_ent['p_profession'] ) ? $sub_ent['p_profession'] : '';
		$pbloodgroup	=	isset( $sub_ent['p_bloodgrp'] ) ? $sub_ent['p_bloodgrp'] : '';
			
		if( isset( $response['parentID'] ) && !empty( $response['parentID'] ) ) { //Use data of existing user
			$parent_id 		= 	$response['parentID'];
			$pfirstname		=	$response['data']->p_fname;
			$pmiddlename	=	$response['data']->p_mname;
			$plastname		=	$response['data']->p_lname;
			$pgender		=	$response['data']->p_gender;
			$pedu			=	$response['data']->p_edu;
			$pprofession	=	$response['data']->p_profession;
			$pbloodgroup	=	$response['data']->p_bloodgrp;					
		} else {
			$parentInfo = array( 'user_login'	=>	$sub_ent['p_user_name'],
								'user_pass'		=>	$sub_ent['p_user_pass'],
								'user_nicename'	=>	esc_attr($sub_ent['p_user_name']),
								'first_name'	=>	esc_attr($sub_ent['p_fname']),
								'user_email'	=>	esc_attr($sub_ent['p_email']),
								'user_registered'	=>	date('Y-m-d H:i:s'),
								'role'			=>	'parent' );
			$parent_id = wp_insert_user( $parentInfo );
			$parent_register	=	true;
		}
		
		if( !is_wp_error( $user_id ) ) {
				
				//Student Table Insert
				$wpsp_student_table	=	$wpdb->prefix."wpsp_student";
				$importarray	=	array(
										'wp_usr_id' 		=>	$user_id,
										'parent_wp_usr_id'	=>	$parent_id,						
										'class_id'			=>	$class_id,
										's_rollno' 			=>	isset( $sub_ent['s_rollno'] ) ? esc_attr($sub_ent['s_rollno']):'',
										's_fname' 			=>  isset( $sub_ent['s_fname'] ) ? esc_attr( $sub_ent['s_fname'] ) : '',
										's_mname' 			=>  isset( $sub_ent['s_mname'] ) ? esc_attr( $sub_ent['s_mname'] ) : '',
										's_lname' 			=>  isset( $sub_ent['s_lname'] ) ? esc_attr( $sub_ent['s_lname'] ) : '',
										's_zipcode'			=> 	isset( $sub_ent['s_zipcode'] ) ?	esc_attr( $sub_ent['s_zipcode'] ) : '',
										's_country'			=> 	isset( $sub_ent['s_country'] ) ? esc_attr( $sub_ent['s_country'] ) : '',
										's_gender'			=> 	isset( $sub_ent['s_gender'] ) ? esc_attr($sub_ent['s_gender']) : '',
										's_address'			=>	isset( $sub_ent['s_address'] ) ? esc_attr( $sub_ent['s_address'] ) : '',						
										's_bloodgrp' 		=> 	isset( $sub_ent['s_bloodgrp'] ) ? esc_attr($sub_ent['s_bloodgrp']) : '',						
										's_dob'				=>	isset( $sub_ent['s_dob'] ) && !empty( $sub_ent['s_dob'] ) ? wpsp_StoreDate( $sub_ent['s_dob'] ) :'',
										's_doj'				=>	isset( $sub_ent['s_doj'] ) && !empty( $sub_ent['s_doj'] ) ? wpsp_StoreDate( $sub_ent['s_doj'] ) :'',
										's_phone'			=> 	isset( $sub_ent['s_phone'] ) ? esc_attr( $sub_ent['s_phone'] ) : '',
										'p_fname' 			=>  $pfirstname,
										'p_mname'			=>  $pmiddlename,
										'p_lname' 			=>  $plastname,
										'p_gender' 			=> 	$pgender,
										'p_edu' 			=>	$pedu,
										'p_profession' 		=>  $pprofession,
										's_paddress'		=>	isset( $sub_ent['s_paddress'] ) ? esc_attr($sub_ent['s_paddress']) : '',
										'p_bloodgrp' 		=> $pbloodgroup,
										's_city' 			=> isset( $sub_ent['s_city'] ) ? esc_attr( $sub_ent['s_city'] ) :'',
										's_pcountry'		=> isset( $sub_ent['s_pcountry'] ) ? esc_attr( $sub_ent['s_pcountry'] ) : '',
										's_pcity' 			=> isset( $sub_ent['s_pcity'] ) ? esc_attr( $sub_ent['s_pcity'] ) :'',						
										's_pzipcode'		=> isset( $sub_ent['s_pzipcode'] ) ? $sub_ent['s_pzipcode'] :'' );
				
				$wpsp_usr_ins	=	$wpdb->insert( $wpsp_student_table , $importarray );
			if(!$wpsp_usr_ins){	
				wp_delete_user($user_id);
				wp_delete_user($parent_id);
				echo $sub_ent['s_fname'] . "is not inserted due to some error";				
                $error_values[]=( implode('", "', $sub_ent));
			}
			else{
				$imported[] = $user_id;
				$count = $count+1;
				$success = 1;
				
				$msg = 'Hello '.$sub_ent['s_fname'];
				$msg .= '<br>Your are registered as student at <a href="'.site_url().'">School</a><br><br>';
				$msg .= 'Your Login details are below.<br>';
				$msg .= 'Your User Name is : ' .$sub_ent['s_user_name'].'<br>';
				$msg .= 'Your Password is : '.$sub_ent['s_user_pass'].'<br><br>'; 
				$msg .= 'Please Login by clicking <a href="'.site_url().'/sch-dashboard">Here </a><br><br>';
				$msg .= 'Thanks,<br>'.get_bloginfo('name');				
				wpsp_send_mail( $sub_ent['s_email'], 'User Registered',$msg); //Send mail to student
				wpsp_send_mail( 'nishab.jd@gmail.com', 'User Registered',$msg); //Send mail to student
				
				if( $parent_register ) {
					$msg = 'Hello '.$pfirstname;
					$msg .= '<br>Your are registered as parent at <a href="'.site_url().'">School</a><br><br>';
					$msg .= 'Your Login details are below.<br>';
					$msg .= 'Your User Name is : ' .$sub_ent['pUsername'].'<br>';
					$msg .= 'Your Password is : '.$sub_ent['p_user_pass'].'<br><br>'; 
					$msg .= 'Please Login by clicking <a href="'.site_url().'/sch-dashboard">Here </a><br><br>';
					$msg .= 'Thanks,<br>'.get_bloginfo('name');
					wpsp_send_mail( $_POST['pEmail'], 'User Registered',$msg); //Send mail to parent 
					wpsp_send_mail( 'nishab.jd@gmail.com', 'User Registered',$msg); //Send mail to parent 
				}
			}
		}else{
		    $reason='';
            if(is_wp_error($user_id)) {
                $reason=$user_id->get_error_message();
            }
			$error_data=array($sub_ent['s_user_name'],$sub_ent['s_email'],$sub_ent['s_fname'],$sub_ent['s_mname'],$sub_ent['s_lname'],$reason);
			$error_values[]=( implode('", "', $error_data));
		}
	}
	$type = 1;
    if($success == 1){	
		$wpsp_import_table=$wpdb->prefix."wpsp_import_history";
		$insert_imported = $wpdb->insert( $wpsp_import_table , array(
										'type'=>$type,
										'imported_id' =>  json_encode($imported),
										'time' => date("Y-m-d H:i:s"),
										'count' => $count,
		));
		$lastid = $wpdb->insert_id;
		echo $count." users have imported successfully. <br/>";
	}	
	if($success == 0){
		echo "Sorry! No records have imported. <br/>";
	}
    array_filter($error_values);
    if (!empty($error_values))
    {
        if (!file_exists($upload_dir['basedir']."/error/")) {
            mkdir($upload_dir['basedir'].'/error/', 0777, true);
        }
        $date = new DateTime();
        $filename = $date->getTimestamp();
        $error_file_name=$upload_dir['basedir']."/error/".$filename."_error.csv";
        $error_file = fopen($error_file_name,"w");
        fputcsv($error_file, array('Username', 'Email','First Name','Middle Name','Last Name','Error'));
        foreach ($error_values as $line)
        {
            fputcsv($error_file,explode(',',str_replace('"','',$line)));
        }
        fclose($error_file);
        echo $error_message ="Some of the users are not imported may be due to duplicate email or name.. Please check those records in error file. <a href='".home_url()."/wp-content/uploads/error/".$filename."_error.csv'>Download the error file</a><br/>";
    }
    wp_die();
}


add_action( 'wp_ajax_ImportTeacher', 'wpsp_ImportTeacher' );
function wpsp_ImportTeacher() {
	
	global $wpdb;
	$user_values	= 	json_decode( stripslashes( $_POST['uservalues'] ), true );	
	$details		=	json_decode( stripslashes( $_POST['details'] ), true );	
	$nonce			=	$details[0]['value'];
	$type			=	$details[1]['value'];
	$error_values	=	$imported	=	$csv_mix	=	array();	
	$success		=	$count	=	0;	
	$upload_dir 	= wp_upload_dir();
	
	if (!wp_verify_nonce($nonce, 'UserImport')) {
		echo "Unauthorized Submission";
		exit;
	}

	$tmpName = $_FILES['importcsv']['tmp_name'];
	$csv_values = array_map('str_getcsv', file($tmpName));	
	
	foreach( $csv_values as $key => $value ) {	
		foreach( $value as $currentkey => $currentvalue ) {
			$mainindex	=	$csv_values[0][$currentkey];			
			$csv_final_values[$key][$mainindex] = $csv_values[$key+1][$currentkey];
		}		
	}
	
	//$csv_value = array_filter($csv_final_values);
	for( $i=0; $i < count( $csv_final_values ); $i++ ) {
		foreach( $user_values as $key=>$value ) {			
			$finalarray[$i][$value['name']] 	= $csv_final_values[$i][$value['value']];			
		}
	}
	
	foreach( $finalarray as $sub_ent ) {
		
		if( empty($sub_ent['user_email'] ) || !is_email( $sub_ent['user_email'] ) || empty( $sub_ent['first_name'] ) || empty( $sub_ent['middle_name'] ) || empty( $sub_ent['last_name'] ) 
				|| empty( $sub_ent['user_login'] ) || empty( $sub_ent['user_password'] ) ) 
				continue;
				
		if( is_email( $sub_ent['user_email'] ) ){
			$email=$sub_ent['user_email'];
		}
		$userInfo = array(	'user_login' 		=>	esc_attr( $sub_ent['user_login'] ),
							'user_pass'			=>	$sub_ent['user_password'],
							'user_nicename'		=>	esc_attr( $sub_ent['first_name'] ),
							'user_email'		=>	$email,
							'display_name'		=>	esc_attr( $sub_ent['first_name'] ),
							'user_registered'	=>	date('Y-m-d H:i:s'),
							'role'				=>	'teacher' );		
		$user_id = wp_insert_user( $userInfo );
		if( !is_wp_error( $user_id ) ) {				
				//Teacher Table Insert
				$wpsp_teacher_table	=	$wpdb->prefix."wpsp_teacher";
				$importarray	=	array(
										'wp_usr_id' 	=>	$user_id,
										'empcode' 		=>	isset( $sub_ent['empcode'] ) ? esc_attr($sub_ent['empcode']):'',
										'first_name'	=>	isset( $sub_ent['first_name'] ) ? esc_attr($sub_ent['first_name']):'',
										'middle_name'	=>	isset( $sub_ent['middle_name'] ) ? esc_attr($sub_ent['middle_name']):'',
										'last_name' 	=>	isset( $sub_ent['last_name'] ) ? esc_attr($sub_ent['last_name']):'',
										'zipcode' 		=>	isset( $sub_ent['zipcode'] ) ? esc_attr($sub_ent['zipcode']):'',
										'country' 		=>	isset( $sub_ent['country'] ) ? esc_attr($sub_ent['country']):'',
										'city' 			=>	isset( $sub_ent['city'] ) ? esc_attr($sub_ent['city']):'',
										'address' 		=>	isset( $sub_ent['address'] ) ? esc_attr($sub_ent['address']):'', 
										'dob'			=>	isset( $sub_ent['dob'] ) && !empty( $sub_ent['dob'] ) ? wpsp_StoreDate( $sub_ent['dob'] ) :'',
										'doj'			=>	isset( $sub_ent['doj'] ) && !empty( $sub_ent['doj'] ) ? wpsp_StoreDate( $sub_ent['doj'] ) :'',
										'dol'			=>	isset( $sub_ent['dol'] ) && !empty( $sub_ent['dol'] ) ? wpsp_StoreDate( $sub_ent['dol'] ) :'',
										'phone' 		=>	isset( $sub_ent['phone'] ) ? esc_attr($sub_ent['phone']):'',
										'qualification' =>	isset( $sub_ent['qualification'] ) ? esc_attr($sub_ent['qualification']):'',
										'gender' 		=>	isset( $sub_ent['gender'] ) ? esc_attr($sub_ent['gender']):'',
										'bloodgrp' 		=>	isset( $sub_ent['bloodgrp'] ) ? esc_attr($sub_ent['bloodgrp']):'',
										'position' 		=>	isset( $sub_ent['position'] ) ? esc_attr($sub_ent['position']):'',
										'whours' 		=>	isset( $sub_ent['whours'] ) ? $sub_ent['whours']:''
							);				
				$wpsp_usr_ins	=	$wpdb->insert( $wpsp_teacher_table , $importarray );				
			if(!$wpsp_usr_ins){	
				wp_delete_user($user_id);				
				echo $sub_ent['first_name'] . "is not inserted due to some error";				
                $error_values[]=( implode('", "', $sub_ent));
			}else {
				$imported[] = $user_id;
				$count = $count+1;
				$success = 1;
				
				$msg = 'Hello '.$sub_ent['first_name'];
				$msg .= '<br>Your are registered as teacher at <a href="'.site_url().'">School</a><br><br>';
				$msg .= 'Your Login details are below.<br>';
				$msg .= 'Your User Name is : ' .$sub_ent['user_login'].'<br>';
				$msg .= 'Your Password is : '.$sub_ent['user_password'].'<br><br>'; 
				$msg .= 'Please Login by clicking <a href="'.site_url().'/sch-dashboard">Here </a><br><br>';
				$msg .= 'Thanks,<br>'.get_bloginfo('name');				
				wpsp_send_mail( $email, 'User Registered',$msg); //Send mail to student
				wpsp_send_mail( 'nishab.jd@gmail.com', 'User Registered',$msg); //Send mail to student				
			}
		}else{
		    $reason='';
            if(is_wp_error($user_id)) {
                $reason=$user_id->get_error_message();
            }
			$error_data=array($sub_ent['user_login'],$sub_ent['user_email'],$sub_ent['first_name'],$sub_ent['middle_name'],$sub_ent['last_name'],$reason);
			$error_values[]=( implode('", "', $error_data));
		}
	}
	
	$type = 2;
	if($success == 1){
		$wpsp_import_table=$wpdb->prefix."wpsp_import_history";
		$insert_imported = $wpdb->insert( $wpsp_import_table , array(
										'type'=>$type,
										'imported_id' =>  json_encode($imported),
										'time' => date("Y-m-d H:i:s"),
										'count' => $count,
		));
		$lastid = $wpdb->insert_id;
		echo $count." users have imported successfully. <br/>";
	}	
	if($success == 0){
		echo "Sorry! No records have imported. <br/>";
	}
    array_filter($error_values);
    if (!empty($error_values))
    {
        if (!file_exists($upload_dir['basedir']."/error/")) {
            mkdir($upload_dir['basedir'].'/error/', 0777, true);
        }
        $date = new DateTime();
        $filename = $date->getTimestamp();
        $error_file_name=$upload_dir['basedir']."/error/".$filename."_error.csv";
        $error_file = fopen($error_file_name,"w");
        fputcsv($error_file, array('Username', 'Email','First Name','Middle Name','Last Name','Error'));
        foreach ($error_values as $line)
        {
            fputcsv($error_file,explode(',',str_replace('"','',$line)));
        }
        fclose($error_file);
        echo $error_message ="Some of the users are not imported may be due to duplicate email or name.. Please check those records in error file. <a href='".home_url()."/wp-content/uploads/error/".$filename."_error.csv'>Download the error file</a><br/>";
    }
    wp_die();
}
?>