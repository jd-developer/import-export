<?php
/*
Plugin Name: 	Import and Export
Plugin URI: 	http://wpschoolpress.com
Description:    Allow user to import,export and print information of teacher, student and parents
Version: 		1.1
Author: 		WpSchoolPress Team
Author URI: 	wpschoolpress.com/team
Text Domain:	WPSchoolPressPro
Domain Path:    languages
@package WPSchoolPressPro
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Basic plugin definitions
 * 
 * @package WPSchoolPress PRO
 * @since 1.1
*/
if( !defined( 'WPSPRO_PLUGIN_URL' ) ) {
	define('WPSPRO_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}

if( !defined( 'WPSPRO_PLUGIN_PATH' ) ) {
	define( 'WPSPRO_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
}

if( !defined( 'WPSPRO_PLUGIN_VERSION' ) ) {
	define( 'WPSPRO_PLUGIN_VERSION', '1.1' ); //Plugin version number
}


add_action( 'plugins_loaded', 'wpspro_plugins_loaded', 999 );
function wpspro_plugins_loaded() {
	
		$wpsp_lang_dir	= dirname( plugin_basename( __FILE__ ) ) . '/languages/';
		load_plugin_textdomain( 'WPSchoolPressPro', false, $wpsp_lang_dir );		
		
		global $wpspro_student, $wpspro_parent, $wpspro_public, $wpsp_pro_version, $wpsp_settings_data, $wpspro_marks, $wpspro_teacher;
		
		//student class handles most of functionalities of 
		include_once( WPSPRO_PLUGIN_PATH . 'includes/wpsp-class-student.php' );
		$wpspro_student = new Wpspro_Student();
		$wpspro_student->add_hooks();		
		
		//parent class handles most of functionalities of 
		include_once( WPSPRO_PLUGIN_PATH . 'includes/wpsp-class-parent.php' );
		$wpspro_parent = new Wpspro_Parent();
		$wpspro_parent->add_hooks();
		
		//teacher class handles most of functionalities of 
		include_once( WPSPRO_PLUGIN_PATH . 'includes/wpsp-class-teacher.php' );
		$wpspro_teacher = new Wpspro_Teacher();
		$wpspro_teacher->add_hooks();
		
		//public class handles most of functionalities of 
		
		
		//public class handles most of functionalities of 
		include_once( WPSPRO_PLUGIN_PATH . 'includes/wpspro-class-marks.php' );
		$wpspro_marks = new Wpspro_Marks();
		$wpspro_marks->add_hooks();
		
		//include misc functions
		include_once( WPSPRO_PLUGIN_PATH . 'includes/wpsp-pro-misc.php' );		

		$wpsp_pro_version = new wpsp_pro_version();
		
		if( !class_exists('Wpsp_Admin') ) {
			
			add_action( 'admin_init', array( $wpsp_pro_version, 'wpsp_pro_plugin_deactivate' ) );
			add_action( 'admin_notices', array( $wpsp_pro_version, 'wpsp_pro_plugin_admin_notice' ) );
		}
	
	
		wp_enqueue_script('WPSPRO_wp_admin_jquery14', WPSPRO_PLUGIN_URL . 'includes/js/wpspro-student-scripts.js', array(
				'jquery'
			) , '1.0.0', true);
			wp_enqueue_script('WPSPRO_wp_admin_jquery15', WPSPRO_PLUGIN_URL . 'includes/js/wpspro-marks-scripts.js', array(
				'jquery'
			) , '1.0.0', true);
			wp_enqueue_script('WPSPRO_wp_admin_jquery16', WPSPRO_PLUGIN_URL . 'includes/js/wpspro-teacher-scripts.js', array(
				'jquery'
			) , '1.0.0', true);
			wp_enqueue_script('WPSPRO_wp_admin_jquery17', WPSPRO_PLUGIN_URL . 'includes/js/wpspro-parent-scripts.js', array(
				'jquery'
			) , '1.0.0', true);	
			
			
			
		
}


class wpsp_pro_version {
	public function __construct() {
		add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'wpspro_update_check' ) );
	}
	

	function wpsp_pro_plugin_admin_notice(){
		 echo '<div class="updated"><p> <strong> To Use PRO Service, Please Activate WPSchoolPress Plugin</strong></p></div>';
         if ( isset( $_GET['activate'] ) )
            unset( $_GET['activate'] );
	}
	
	function wpsp_pro_plugin_deactivate() {
		 deactivate_plugins( plugin_basename( __FILE__ ) );
	}
	
	function wpspro_update_check($transient) {

		// Check if the transient contains the 'checked' information
		// If no, just return its value without hacking it
		if ( empty( $transient->checked ) )
			return $transient;

		$plugin_path = plugin_basename( __FILE__ );

		// POST data to send to your API
		$args = array(
			'referrer' 	=> 	get_site_url(),
			'code' 		=>	get_option('wpsp-lcode')
		);

		// Send request checking for an update
		$response = $this->wpspro_updateInfo( $args );
		$response=json_decode($response,true);

		$obj 				=	(object) array();
		$obj->slug 			=	'wpschoolpress-pro';
		$obj->new_version 	= 	$response['new_version'];
		$obj->tested 		=	$response['tested'];
		$obj->url 			= 	$response['url'];
		$obj->package 		=	$response['package'];
		// If there is a new version, modify the transient
		if( version_compare( $response['new_version'], $transient->checked[$plugin_path], '>' ) )
			$transient->response[$plugin_path] = $obj;
		return $transient;
	}
	
	function wpspro_updateInfo( $args ) {
		// Send request
		$request = wp_remote_post('http://wpschoolpress.com/update', array( 'method' => 'POST','body' => $args ) );
		if ( is_wp_error( $request ) || 200 != wp_remote_retrieve_response_code( $request ) )
			return false;
		return wp_remote_retrieve_body( $request ) ;
	}
}
